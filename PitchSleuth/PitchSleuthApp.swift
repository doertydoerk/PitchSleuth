//
//  PitchSleuthApp.swift
//  PitchSleuth
//
//  Created by Dirk Gerretz on 01.12.23.
//

import SwiftUI

@main
struct PitchSleuthApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
